# makefile is only for linux base systems
# can be run by having make + command e.g make vue_dev

vue_dev:
	rm -rf app/assets/webpack_bundles/*.js;
	cd frontend; npm run build_dev; cd -;
	mkdir -p app/assets/webpack_bundles/;
	mv app/assets/webpack_bundles_build/*.js app/assets/webpack_bundles/;
	make assets

vue:
	rm -rf app/assets/webpack_bundles/*.js;
	rm -rf app/assets/webpack_bundles/*.map;
	cd frontend; npm run build; cd -;
	mkdir app/assets/webpack_bundles/;
	mv app/assets/webpack_bundles_build/*.js app/assets/webpack_bundles/;
	mv app/assets/webpack_bundles_build/*.map app/assets/webpack_bundles/;
	make assets

assets:
	python3 manage.py collectstatic --no-input -v 0

shell:
	python3 manage.py shell

migrations:
	python3 manage.py makemigrations

migrate:
	python3 manage.py migrate

jsreverse:
	python3 manage.py collectstatic_js_reverse

superuser:
	python3 manage.py createsuperuser

startapp:
	python3 manage.py startapp $(APP)

serve:
	gunicorn --bind 0.0.0.0:3000 app.wsgi_dev:application
