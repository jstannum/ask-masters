del /q /f /s "app\assets\webpack_bundles\*.js"
del /q /f /s "app\assets\webpack_bundles_build\*.js"
del /q /f /s "app\static\webpack_bundles\*.js"
del /q /f /s "app\static\webpack_bundles_build\*.js"
cd frontend
call npm run build_dev
cd ..
mkdir "app\assets\webpack_bundles\"
move /y "app\assets\webpack_bundles_build\*.js" "app\assets\webpack_bundles\"
python manage.py collectstatic --no-input -v 0
