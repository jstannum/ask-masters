# Ask Meow

## Technologies Used
- [VueJS](https://vuejs.org/) (2.x) as the frontend Javascript library
- [Webpack](https://webpack.js.org/) Build tool for the frontend
- [Django](https://www.djangoproject.com/) as the backend Web Framework
- [Postgres](https://www.postgresql.org/) as the database stored on [ElephantSQL](elephantsql.com)

## Project setup

### create your virtualenv
```
pip3 install virtualenv # if virtualenv is not yet installed
virtualenv -p python3 .venv
```
Note: if you have set your python default version to 3 you can use `pip` and `python` command instead

### install the packages using the virtual environment
For linux based terminal
```
source .venv/bin/activate
pip3 install -r requirements.txt
```
For windows command line
```
cd .venv/bin && ./activate.bat && cd ..
pip3 install -r requirements_win.txt
```

### setting up vue
```
cd frontend && npm i && cd ..
mkdir -p app/assets/webpack_bundles_build
touch app/assets/webpack_bundles_build/test.js
make vue_dev
```

### starting the server

```
python3 manage.py runserver
```
