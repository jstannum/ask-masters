#!/usr/bin/env sh
set -x
gunicorn --bind 0.0.0.0:3000 app.wsgi_dev:application &
sleep 1
echo $! > .pidfile
set +x

echo 'Now...'
echo 'Visit http://localhost:3000 to see your Django application in action.'
echo '(This is why you specified the "args ''-p 3000:3000''" parameter when you'
echo 'created your initial Pipeline as a Jenkinsfile.)'