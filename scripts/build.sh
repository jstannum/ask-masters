#!/usr/bin/env bash
# exit on error
set -o errexit

pip3 install -r requirements.txt

cd frontend && npm i && cd ..
mkdir -p app/assets/webpack_bundles_build
touch app/assets/webpack_bundles_build/test.js
make vue_dev
make migrate
