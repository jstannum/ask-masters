/**
 * Retrieve the url parameer as object by keys
 * @param {String} query_param_str Custom query param string by default it will use the current url params
 */
export const getQueryParams = function (query_param_str) {
  if (!query_param_str) query_param_str = window.location.search.substring(1)

  return query_param_str.split('&')
    .reduce((params, param) => {
      let [key, value] = param.split('=')
      params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')).trimEnd() : ''
      return params
    }, {})
}