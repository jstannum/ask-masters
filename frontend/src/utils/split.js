import {SplitFactory} from '@splitsoftware/splitio'

const SPLIT_AUTH_KEY = '3e06jsg4ln2d09rh8mmmio8t8vul8unntq7n'

class SplitSDK {
    constructor() {
        this.isReady = false
        this.factory = SplitFactory({ 
            core: {
              authorizationKey: SPLIT_AUTH_KEY,
              key: 'key'
            }
          })
        this.client = this.factory.client();
        this.client.on(this.client.Event.SDK_READY, () => {
          this.isReady = true
        })
    }

    getTreatment(name) {
      if (!this.isReady) {
        return
      }
      return this.client.getTreatment(name);
    }
}
const split = new SplitSDK()
export default split
