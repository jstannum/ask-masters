import Vue from 'vue'

import Questions from './Questions.vue'
import store from './store/index.js'

new Vue({
  el: '#app-questions',
  store: store,
  render: h => h(Questions)
})
