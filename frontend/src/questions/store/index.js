import Vue from 'vue'
import Vuex from 'vuex'
import * as getters from './getters.js'
import * as actions from './actions.js'
import * as types from './mutation-types.js'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const state = {
  split: {},
}

const mutations = {
  [types.SET_SPLIT] (state, payload) {
    state.split = payload.split
  },
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  strict: debug,
})
