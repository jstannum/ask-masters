import axios from 'axios'

import * as types from './mutation-types.js'

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

export const setSplit = ({commit}, payload) => {
  commit(types.SET_SPLIT, {
    split: payload.split
  })
}