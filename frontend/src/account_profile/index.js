import Vue from 'vue'

import AccountProfile from './AccountProfile.vue'

new Vue({
  el: '#app',
  render: h => h(AccountProfile)
})
