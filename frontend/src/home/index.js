import Vue from 'vue'

import Home from './Home.vue'

new Vue({
  el: '#app-home',
  render: h => h(Home)
})
