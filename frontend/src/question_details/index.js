import Vue from 'vue'

import QuestionDetails from './QuestionDetails.vue'

new Vue({
  el: '#app-question-details',
  render: h => h(QuestionDetails)
})
