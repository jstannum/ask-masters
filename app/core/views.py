from django.shortcuts import render
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class GenericView(View):
    """
    Base view that every view should inherit from.
    Adds a default `user` instance to the context data
    """
    template_name = 'base.html'

    def get_context_data(self, **kwargs):
        context = {}
        context.update(**kwargs)
        return context

    def get(self, request, *args,  **kwargs):
        context = self.get_context_data(**kwargs)
        return render(request, self.template_name, context)


class LoginGenericView(LoginRequiredMixin, GenericView):
    """
    Any core view that requires a login redirect should inherit from.
    """
    login_url = 'account_login'
