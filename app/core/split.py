from __future__ import absolute_import, division, print_function, unicode_literals

from django.conf import settings
from splitio.factories import get_factory as get_splitio_factory
from splitio.exceptions import TimeoutException

import sys


class SplitSDK:

    def __init__(self):
        # Get config from Django settings
        sdk_config = settings.SPLITIO
        api_key = settings.SPLITIO.get('apiKey', '')

        self.factory = get_splitio_factory(
            api_key,
            config=sdk_config,
            **{k: sdk_config[k] for k in ('sdk_api_base_url', 'events_api_base_url', 'auth_api_base_url', 'streaming_api_base_url') if k in sdk_config}
        )
        self.client = None

    def get_client(self):
        if not self.client:
            try:
                self.factory.block_until_ready(5)
            except TimeoutException:
                # The SDK failed to initialize in 5 seconds. Abort!
                sys.exit()

            self.client = self.factory.client()

        return self.client

    def get_treatment(self, uid, treatment_name):
        client = self.client
        if not client:
            client = self.get_client()

        return client.get_treatment(uid, treatment_name)
