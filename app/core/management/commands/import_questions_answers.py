from app.questions.models import (
    Question,
    Answer,
    Tag,
)
from ..base import JsonBaseImportCommand


class Command(JsonBaseImportCommand):
    model = Answer

    def get_object(self, data):
        question = Question.objects.filter(id=data['id']).first()
        if not question:
            question = Question.objects.create(
                id=data['id'],
                title=data['title'],
                content=data['content'],
                creator_name=data['created_by'] or '',
                created_time=data['created_time'],
            )

        for data_tag in data['tags']:
            tag = Tag.objects.filter(name=data_tag).first()
            if tag:
                question.tags.add(tag)

        for data_answer in data['answers']:
            answer = Answer.objects.create(
                id=data_answer['id'],
                question=question,
                answer=data_answer['content'],
                creator_name=data_answer['created_by'] or '',
                created_time=data_answer['created_time'],
            )

            if data_answer['is_accepted']:
                question.accepted_answer = answer
                question.save()

        return (question, True)

    def get_objects(self, data):
        objects = []

        for row in data:
            self.get_object(row)

        return objects
