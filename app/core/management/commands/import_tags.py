from app.questions.models import Tag
from ..base import JsonBaseImportCommand


class Command(JsonBaseImportCommand):
    model = Tag

    def get_object(self, data):
        tag = self.model(
            name=data['name']
        )

        return (tag, True)
