from django.core.management.base import BaseCommand
from django.contrib.sites.models import Site


class Command(BaseCommand):
    help = 'Create site on the database'

    def handle(self, *args, **options):
        Site.objects.get_or_create(
            domain='askmeow.com',
            name='askmeow.com',
        )
