import json

from django.core.management.base import BaseCommand, CommandError


class JsonBaseImportCommand(BaseCommand):
    """Base command class for data import commands.

    Attrs:
        model (django.db.models.Model): The model to be used for creation of records.
    """
    model = None

    def add_arguments(self, parser):
        parser.add_argument(
            'json_file',
            type=str,
            help='json file to import',
        )

    def get_object(self, data):
        """Implement this method in the main import command class.

        Args:
            data (dict): dict representation of a json row.

        Should return an instance of `self.model`.
        """
        raise NotImplementedError()

    def get_objects(self, data):
        objects = []

        for row in data:
            obj, create = self.get_object(row)

            if not obj:
                continue

            if create:
                objects.append(obj)
            else:
                self.perform_update(obj, row)

        return objects

    def handle(self, *args, **options):
        if not self.model:
            raise CommandError('model is not configured')

        filename = options.get('json_file')

        try:
            with open(filename, 'r', encoding='utf-8') as jsonfile:
                reader = json.load(jsonfile)
                objects = self.get_objects(reader)
                self.perform_import(objects)
                self.stdout.write(self.style.SUCCESS('Import successful'))
        except FileNotFoundError:
            self.stderr.write(self.style.ERROR(
                'File not found: {}'.format(filename)))

    def perform_update(self, obj, data):
        """Hook for updating objects"""
        return obj

    def perform_import(self, objects):
        self.model.objects.bulk_create(objects)
