from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import (
    ListAPIView,
    ListCreateAPIView,
    RetrieveUpdateAPIView,
)
# from django_filters.rest_framework import DjangoFilterBackend

from app.questions.models import (
    Question,
)

from ..serializers.questions import (
    QuestionSerializer,
)


class QuestionListCreateAPIView(ListCreateAPIView):
    serializer_class = QuestionSerializer
    filter_backends = (
        SearchFilter,
        OrderingFilter,
    )
    search_fields = (
        'title',
        'content',
        'tags__name',
    )
    ordering_fields = (
        'title',
        'created_time',
        'modified_time',
    )

    def get_queryset(self, **kwargs):
        qs = Question.objects.all()
        tags = self.request.query_params.get('tags[]')
        if tags:
            qs = qs.filter(
                tags__id__in=tags,
            )
        return qs
