from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path, include

from ..views.questions import (
    QuestionListCreateAPIView,
)

question_patterns = format_suffix_patterns([
    path(
        '',
        QuestionListCreateAPIView.as_view(),
        name='all',
    ),
])
