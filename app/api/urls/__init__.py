from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path, include

from .questions import question_patterns

app_name = 'api'

urlpatterns = [
    path(
        'questions/',
        include((question_patterns, 'questions'))
    ),
]
