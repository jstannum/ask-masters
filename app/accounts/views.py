from django.shortcuts import render
from app.core.views import GenericView
# Create your views here.


class HomeView(GenericView):
    template_name = 'accounts/home.html'
