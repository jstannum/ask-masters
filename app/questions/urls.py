from django.urls import path

from .views import (
    QuestionsView,
    QuestionDetailsView,
)

app_name = 'questions'


urlpatterns = [
    path(
        '',
        QuestionsView.as_view(),
        name='questions',
    ),
    path(
        '<int:pk>',
        QuestionDetailsView.as_view(),
        name='questions',
    ),
]
