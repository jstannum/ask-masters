from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from app.core.models import BaseModel


class Question(BaseModel):
    title = models.CharField(max_length=100)
    content = models.TextField()
    tags = models.ManyToManyField(
        'questions.Tag',
        related_name='questions',
    )
    accepted_answer = models.ForeignKey(
        'questions.Answer',
        on_delete=models.SET_NULL,
        related_name='answer_to_question',
        null=True,
    )

    # stackoverflow imported fields
    creator_name = models.CharField(
        max_length=100,
        blank=True,
        default='',
        help_text='This is only for imported data',
    )

    def __str__(self):
        return self.title


class Answer(BaseModel):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.TextField()

    # stackoverflow imported fields
    creator_name = models.CharField(
        max_length=100,
        blank=True,
        default='',
        help_text='This is only for imported data',
    )

    def __str__(self):
        return self.answer[:75]


class Comment(BaseModel):
    text = models.TextField()

    # dynamic foreign key
    # https://docs.djangoproject.com/en/dev/ref/contrib/contenttypes/#generic-relations
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.text


class Tag(BaseModel):
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name
