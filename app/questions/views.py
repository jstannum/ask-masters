from app.core.views import GenericView


class QuestionsView(GenericView):
    template_name = 'questions/index.html'


class QuestionDetailsView(GenericView):
    template_name = 'questions/details.html'
    pk_url_kwarg = 'pk'
